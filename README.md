# Unreal Engine C++ State Machines

This plugin allows to create state machines in both C++ and Blueprint.
## Add plugin to existing project

Go to the project's plugins directory.

```bash
  {PROJECT_NAME}/Plugins
```

Clone this repo in the plugins directory.

```bash
  git clone https://gitlab.com/c1696/ue-cpp-state-machines.git
```

Go back to the project directory and generate Visual Studio project files. Then build the project.

In the editor go to plugins and enable "Unreal Engine CPP State Machine".
## Authors

- [@darmane](https://gitlab.com/darmane)


## Related

This project is based in these ones.

- [Monster25 -> TagStateMachines](https://github.com/Monster25/TagStateMachine)


## License

MIT License.

You can fork this project for free under the following conditions:

- Add a link to this project.
- The software is provided on an "as is" and without warranty of any kind. Use it at your own risk.
