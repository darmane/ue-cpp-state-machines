// Copyright 2022 - With love by Confused Studio 

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameplayTags.h"
#include "TagsStateMachineComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FStateChangedSignature, const FGameplayTag&, NewStateTag);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInitStateSignature, const FGameplayTag&, StateTag);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEndStateSignature, const FGameplayTag&, StateTag);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FTickStateSignature, float, DeltaTime, const FGameplayTag&, StateTag);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable, BlueprintType )
class UECPPSTATEMACHINES_API UTagsStateMachineComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTagsStateMachineComponent();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FGameplayTag CurrentStateTag;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FGameplayTag InitialStateTag;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<FGameplayTag> StateHistory;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	int32 StateHistoryLength = 5;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool bDebug = false;


	// DELEGATES
	UPROPERTY(BlueprintAssignable)
	FInitStateSignature InitStateDelegate;

	UPROPERTY(BlueprintAssignable)
	FEndStateSignature EndStateDelegate;

	UPROPERTY(BlueprintAssignable)
	FStateChangedSignature StateChangedDelegate;

	UPROPERTY(BlueprintAssignable)
	FTickStateSignature TickStateDelegate;

private:
	bool bCanTickState = false;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public: 
	UFUNCTION(BlueprintCallable)
	bool SwitchState(FGameplayTag NewStateTag);

private:
	void InitState();
	void TickState(float DeltaTime);
	void EndState();
		
};
