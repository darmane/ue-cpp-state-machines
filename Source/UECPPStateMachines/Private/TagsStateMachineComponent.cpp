// Copyright 2022 - With love by Confused Studio 
// This code is based in Monster25's code, here is his repo -> https://github.com/Monster25/TagStateMachine.git


#include "TagsStateMachineComponent.h"

// Sets default values for this component's properties
UTagsStateMachineComponent::UTagsStateMachineComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTagsStateMachineComponent::BeginPlay()
{
	Super::BeginPlay();

	// Initialize state machine
	SwitchState(InitialStateTag);
}


// Called every frame
void UTagsStateMachineComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bCanTickState) {
		TickState(DeltaTime);
	}

	if (bDebug) {
		GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Red, FString::Printf(TEXT("TagStateMachine - Current State for %s: %s"), *GetOwner()->GetName(), *CurrentStateTag.ToString()));

		if (StateHistory.Num() > 0)
		{
			for (int32 i = 0; i < StateHistory.Num(); i++)
			{
				GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Red, FString::Printf(TEXT("TagStateMachine - %s"), *StateHistory[i].ToString()));
			}
		}
	}
}

bool UTagsStateMachineComponent::SwitchState(FGameplayTag NewStateTag)
{
	if (!NewStateTag.MatchesTagExact(CurrentStateTag)) {
		bCanTickState = false;

		EndState();

		CurrentStateTag = NewStateTag;
		InitState();

		bCanTickState = true;
		if (StateChangedDelegate.IsBound()) {
			StateChangedDelegate.Broadcast(CurrentStateTag);
		}

		return true;
	}
	else {
		if (bDebug) {
			UE_LOG(LogTemp, Warning, TEXT("TagStateMachine - Couldn't switch state for %s because it is already in %s"), *GetOwner()->GetName(), *NewStateTag.ToString());
		}

		return false;
	}
}

void UTagsStateMachineComponent::InitState()
{
	if (InitStateDelegate.IsBound()) {
		InitStateDelegate.Broadcast(CurrentStateTag);
	}
}

void UTagsStateMachineComponent::TickState(float DeltaTime)
{
	if (TickStateDelegate.IsBound()) {
		TickStateDelegate.Broadcast(DeltaTime, CurrentStateTag);
	}
}

void UTagsStateMachineComponent::EndState()
{
	if (StateHistory.Num() >= StateHistoryLength) {
		StateHistory.RemoveAt(0);
	}
	StateHistory.Push(CurrentStateTag);

	if (EndStateDelegate.IsBound()) {
		EndStateDelegate.Broadcast(CurrentStateTag);
	}
}

